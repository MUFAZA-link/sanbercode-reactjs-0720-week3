import React, {Component} from "react"
import "./Lists.css"

class Lists extends Component{

  constructor(props){
    super(props)
    this.state = {
     DaftarBuah : [
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}
    ],
     inputNama : "",
     inputHarga : "",
     inputBerat : "",
     /// array tidak punya index -1
     indexOfForm: -1    
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
 
  handleDelete(event){
    let index = event.target.value
    let newDaftarBuah = this.state.DaftarBuah
    let editedDataBuah = newDaftarBuah[this.state.indexOfForm]
    newDaftarBuah.splice(index, 1)

    if (editedDataBuah !== undefined){
      // array findIndex baru ada di ES6
      var newIndex = newDaftarBuah.findIndex((el) => el === editedDataBuah)
      this.setState({DaftarBuah: newDaftarBuah, indexOfForm: newIndex})
      
    }else{
      
      this.setState({DaftarBuah: newDaftarBuah})
    }
    
  }
  
  handleEdit(event){
    let index = event.target.value
    let buah = this.state.DaftarBuah[index]
    console.log('index di handleEdit',index);
    console.log('buah di handleEdit',buah);
    let stateEdit = {
      inputNama: buah.nama,
      inputHarga: buah.harga,
      inputBerat: buah.berat,
      indexOfForm: index 
    }
    this.setState(stateEdit)
  }

  // method with arrow function
  handleChangeNama = (event) => {
    console.log('event in handleChangeNama',event);
    this.setState({inputNama: event.target.value});
  }
  handleChangeHarga = (event) => {
    console.log('event in handlechangeharga',event);
    this.setState({inputHarga: event.target.value});
  }
  handleChangeBerat = (event) => {
    console.log('event in handlechangeberat',event);
    this.setState({inputBerat: event.target.value});
  }

  handleSubmit(event){
    // menahan submit
    event.preventDefault()

    let name = this.state.inputNama;
    let harga = this.state.inputHarga;
    let berat = this.state.inputBerat;

    let objBuah = {nama: name, harga: harga, berat: berat};
    if (name.replace(/\s/g,'') !== ""){      
      let newDaftarBuah = this.state.DaftarBuah
      let index = this.state.indexOfForm
      
      if (index === -1){
        newDaftarBuah = [...newDaftarBuah, objBuah]
      }else{
        newDaftarBuah[index] = objBuah
      }
  
      this.setState({
        DaftarBuah: newDaftarBuah,
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
      })
    }

  }

  render(){
    return(
      <>
        <h1>Daftar Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
              {
                this.state.DaftarBuah.map((val, index)=>{
                  return(                    
                    <tr key={index}>
                      <td>{index+1}</td>
                      <td>{val.nama}</td>
                      <td>{val.harga}</td>
                      <td>{val.berat/1000} kg</td>
                      <td>
                        <button onClick={this.handleEdit} value={index}>Edit</button>
                        &nbsp;
                        <button onClick={this.handleDelete} value={index}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
        {/* Form */}
        <h1>Form Buah</h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Masukkan Nama Buah:
          </label>          
          <input type="text" value={this.state.inputNama} onChange={this.handleChangeNama}/>
          <label>
            Masukkan Harga Buah:
          </label>          
          <input type="text" value={this.state.inputHarga} onChange={this.handleChangeHarga}/>
          <label>
            Masukkan Berat:
          </label>          
          <input type="text" value={this.state.inputBerat} onChange={this.handleChangeBerat}/>
          <button>submit</button>
        </form>
      </>
    )
  }
}

export default Lists
