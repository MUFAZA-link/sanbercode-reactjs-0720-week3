import React,{ useState, useEffect } from "react";
import axios from "axios";

const Lists = () => { 
  // const arrayBuah = [
  //   {nama: "Semangka", harga: 10000, berat: 1000},
  //   {nama: "Anggur", harga: 40000, berat: 500},
  //   {nama: "Strawberry", harga: 30000, berat: 400},
  //   {nama: "Jeruk", harga: 30000, berat: 1000},
  //   {nama: "Mangga", harga: 30000, berat: 500}
  // ];
  const arrayBuah = null;
  const [daftarBuah,setDaftarBuah] = useState(null);
  const [inputNama,setInputNama] = useState('');
  const [inputHarga,setInputHarga] = useState('');
  const [inputBerat,setInputBerat] = useState('');
  const [indexOfForm,setIndexOfForm] = useState(-1);
  const [temp,setTemp] = useState(null);

  const handleDelete = (event) => {
    let index = event.target.value
    let newDaftarBuah = daftarBuah
    let editedDataBuah = newDaftarBuah[indexOfForm]
    newDaftarBuah.splice(index, 1)

    console.log('daftartbuah delete',newDaftarBuah);
    if (editedDataBuah !== undefined){
      // array findIndex baru ada di ES6
      var newIndex = newDaftarBuah.findIndex((el) => el === editedDataBuah)
      setDaftarBuah([...newDaftarBuah]) 
      setIndexOfForm(newIndex)
      
    }else{
      
      setDaftarBuah([...newDaftarBuah])
    }
    
  }
  
  const handleEdit = (event) =>{
    let index = event.target.value
    console.log('index',index);
    console.log(daftarBuah);
    let itemBuah = daftarBuah.filter((el) => el.id == index)
    console.log(itemBuah);
    setInputNama(itemBuah[0].name);
    setInputHarga(itemBuah[0].price);
    setInputBerat(itemBuah[0].weight);
    setIndexOfForm(index)
    
  }

  const handleChangeNama = (event) => {
    console.log('event in handleChangeNama',event);
    setInputNama(event.target.value);
  }
  const handleChangeHarga = (event) => {
    console.log('event in handlechangeharga',event);
    setInputHarga(event.target.value);
  }
  const handleChangeBerat = (event) => {
    console.log('event in handlechangeberat',event);
    setInputBerat(event.target.value);
  }

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let name = inputNama;
    let harga = inputHarga;
    let berat = inputBerat;

    let objBuah = {name: name, price: harga, weight: berat};

    if (name.replace(/\s/g,'') !== ""){      
      let newDaftarBuah = daftarBuah
      let index = indexOfForm
      
      if (index === -1){
        axios.post(`http://backendexample.sanbercloud.com/api/fruits/`,objBuah)
          .then(response => {
            console.log('response',response);
            // setDaftarBuah(response.data);
          })
      }else{
        // newDaftarBuah[index] = objBuah
          axios.put(`http://backendexample.sanbercloud.com/api/fruits/${index}`,objBuah)
          .then(response => {
            console.log('response',response);
            // setDaftarBuah(response.data);
          })
      }
  
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(response => {
      console.log('response',response);
      setDaftarBuah(response.data);
      setInputNama("")
      setInputHarga("")
      setInputBerat("")
    })
      
    }

  }

  useEffect(() => {
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(response => {
      console.log('response',response);
      setDaftarBuah(response.data);
    })
  },[])

  if (daftarBuah) {
    return(
      <>
        <h1>Daftar Harga Buah</h1>
          <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {
                daftarBuah.map((val, index)=>{
                  return(                    
                    <tr key={val.id}>
                      <td>{val.id}</td>
                        <td>{val.name}</td>
                        <td>{val.price}</td>
                        <td>{val.weight/1000} kg</td>
                        <td>
                        <button onClick={handleEdit} value={val.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleDelete} value={val.id}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
        {/* Form */}
        <h1>Form Peserta</h1>
        <form onSubmit={handleSubmit}>
          <label>
            Masukkan nama peserta:
          </label>          
          <input type="text" value={inputNama} onChange={handleChangeNama}/>
          <label>
            Masukkan nama peserta:
          </label>          
          <input type="text" value={inputHarga} onChange={handleChangeHarga}/>
          <label>
          Masukkan Berat:
          </label>          
          <input type="text" value={inputBerat} onChange={handleChangeBerat}/>
          <button>submit</button>
        </form>
      </>
    )
  } else {
    return (
      <>
      <p>Kosong</p>
      </>
    )
  }
}

export default Lists